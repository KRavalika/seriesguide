import { TestBed } from '@angular/core/testing';

import { GenredramaDetailsService } from './genredrama-details.service';

describe('GenredramaDetailsService', () => {
  let service: GenredramaDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenredramaDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
