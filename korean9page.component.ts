import { Component, OnInit } from '@angular/core';
import { KoreadramaDetailsService } from '../services/koreadrama-details.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-korean9page',
  templateUrl: './korean9page.component.html',
  styleUrls: ['./korean9page.component.css']
})
export class Korean9pageComponent implements OnInit {
  constructor(private param:ActivatedRoute,private service:KoreadramaDetailsService) { }
  getdrId:any;
  adData:any;

  ngOnInit(): void {
    this.getdrId = this.param.snapshot.paramMap.get('id');
    console.log(this.getdrId,'getmenu');
    if(this.getdrId)
    {
      this.adData =  this.service.Koreadramadetails.filter((value)=>{
          return value.id == this.getdrId;
        });
        console.log(this.adData,'addata>>');
        
    }
    
  }

}