import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GenrdramaDetailsService } from 'src/app/services/genrdrama-details.service';

@Component({
  selector: 'app-gener9page',
  templateUrl: './gener9page.component.html',
  styleUrls: ['./gener9page.component.css']
})
export class Gener9pageComponent implements OnInit {
  constructor(private param:ActivatedRoute,private service:GenrdramaDetailsService) { }
  getdrId:any;
  adData:any;

  ngOnInit(): void {
    this.getdrId = this.param.snapshot.paramMap.get('id');
    console.log(this.getdrId,'getmenu');
    if(this.getdrId)
    {
      this.adData =  this.service.genrdramadetails.filter((value)=>{
          return value.id == this.getdrId;
        });
        console.log(this.adData,'addata>>');
        
    }
    
  }

}
