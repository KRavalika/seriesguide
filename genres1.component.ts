import { Component, OnInit } from '@angular/core';
import { GenredramaDetailsService } from '../services/genredrama-details.service';

@Component({
  selector: 'app-genres1',
  templateUrl: './genres1.component.html',
  styleUrls: ['./genres1.component.css']
})
export class Genres1Component implements OnInit {
  constructor(private service:GenredramaDetailsService) { }
  adData:any;
  ngOnInit(): void 
  {
    this.adData = this.service.gendramaDetails;
  }

}

