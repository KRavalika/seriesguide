import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActiondramaDetailsService } from 'src/app/services/actiondrama-details.service';

@Component({
  selector: 'app-action9page',
  templateUrl: './action9page.component.html',
  styleUrls: ['./action9page.component.css']
})

export class Action9pageComponent implements OnInit {

  constructor(private param:ActivatedRoute,private service:ActiondramaDetailsService) { }
  getdrId:any;
  adData:any;

  ngOnInit(): void {
    this.getdrId = this.param.snapshot.paramMap.get('id');
    console.log(this.getdrId,'getmenu');
    if(this.getdrId)
    {
      this.adData =  this.service.actiondramaDetails.filter((value)=>{
          return value.id == this.getdrId;
        });
        console.log(this.adData,'addata>>');
        
    }
    
  }

}